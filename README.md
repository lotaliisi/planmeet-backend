# PlanMeet backend

PlanMeet is application for TalTech student organizations to plan their meetings. It was created as final bachelor's thesis project for Tallinn University of Technology. Application contains of two parts - backend and frontend.

Backend can be found in: https://bitbucket.org/lotaliisi/planmeet-backend

Frontend can be found in: https://bitbucket.org/lotaliisi/planmeet-frontend

## Project technologies in backend
- Java as programming languages.
- Spring Boot as main framework.
- Spring Data JPA for database connection.
- Gradle as project building tool. 
- MariaDb as database.

## Running PlanMeet application
PlanMeet application needs frontend, backend and database to work. 
Instructions to set up backend and database can be found here and 
instructions to set up frontend can be found in 
[PlanMeet frontend](https://bitbucket.org/lotaliisi/planmeet-frontend).

Before running backend you should set up MariaDb server and create empty 
database for PlanMeet application. Next you should write your database connection 
information into ```application.properties``` file. Application will add tables 
and some initial content into database when application starts. At the moment logging in to 
Study Information System ÕIS is not part of application workflow so username and 
password have to be written into properties also. Example of
[application.properties](https://bitbucket.org/lotaliisi/planmeet-backend/src/master/src/main/resources/application.properties).
You should fill fields with ***.

- spring.datasource.url - url where your database runs ```jdbc:mariadb://[host]:[port]/[databaseName]```,
  example: ```jdbc:mariadb://127.0.0.1:3306/planmeet```
- spring.datasource.username - username to access database
- spring.datasource.password - password to access database
- jwtSecret - write secret for JWT
- oisUser - your ÕIS Uni-ID
- oisPswd - your ÕIS Uni-ID password

On first run application will create tables and initialize some data. To do this
[application.properties](https://bitbucket.org/lotaliisi/planmeet-backend/src/master/src/main/resources/application.properties)
file has to include parameters:

- spring.jpa.hibernate.ddl-auto=create-drop
- spring.datasource.initialization-mode=always

On second run you should change these to avoid dropping and creating tables and 
creating same data twice:

- spring.jpa.generate-ddl=true
- spring.jpa.hibernate.ddl-auto=update
- spring.datasource.initialization-mode=never

### Commands
Below you can find useful commands to run PlanMeet backend project.

#### Downloading project from Bitbucket
```
git clone https://lotaliisi@bitbucket.org/lotaliisi/planmeet-backend.git
```
#### Running
Starts Tomcat Web Server and application from main method in 
[PlanMeetApplication.java](https://bitbucket.org/lotaliisi/planmeet-backend/src/master/src/main/java/com/PlanMeet/PlanMeetApplication.java).
```
gradlew bootRun
```

## Logging in to PlanMeet
When application is running then it is possible to log in with credentials below:

For user: 

- username: user
- password: test

For admin:

- username: admin
- password: test
