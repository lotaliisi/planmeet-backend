package com.PlanMeet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanMeetApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlanMeetApplication.class, args);
    }

}
