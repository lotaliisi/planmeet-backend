package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Organization;
import org.springframework.data.repository.CrudRepository;

public interface OrganizationRepository extends CrudRepository<Organization, Integer> {
    Organization findByName(String name);
}
