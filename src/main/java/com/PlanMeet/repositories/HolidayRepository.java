package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Holiday;
import org.springframework.data.repository.CrudRepository;

public interface HolidayRepository extends CrudRepository<Holiday, Integer> {
    Holiday findTopByOrderByIdDesc();
}
