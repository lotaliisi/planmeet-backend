package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.MeetingMember;
import org.springframework.data.repository.CrudRepository;

public interface MeetingMemberRepository extends CrudRepository<MeetingMember, Integer> {
    MeetingMember findByUser_IdAndMeeting_Id(Integer userId, Integer meetingId);

    MeetingMember[] findAllByMeeting_Id(Integer meetingId);

    MeetingMember[] findAllByUser_Id(Integer userId);
}
