package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.MemberAnswer;
import org.springframework.data.repository.CrudRepository;

public interface MemberAnswerRepository extends CrudRepository<MemberAnswer, Integer> {
    MemberAnswer[] findAllByMeetingMember_Meeting_Id(Integer meetingId);

    MemberAnswer[] findAllByMeetingMember_Id(Integer meetingMemberId);

    MemberAnswer findByMeetingTime_IdAndMeetingMember_Id(
            Integer meetingTimeId,
            Integer meetingMemberId);

    MemberAnswer[] findAllByMeetingMember_User_IdAndMeetingMember_Meeting_Id(
            Integer userId,
            Integer meetingId);
}
