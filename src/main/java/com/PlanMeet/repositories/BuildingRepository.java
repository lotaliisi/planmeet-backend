package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Building;
import org.springframework.data.repository.CrudRepository;

public interface BuildingRepository extends CrudRepository<Building, Integer> {
    Building findByName(String name);
}
