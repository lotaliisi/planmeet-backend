package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.OrganizationHoliday;
import org.springframework.data.repository.CrudRepository;

public interface OrganizationHolidayRepository extends CrudRepository<OrganizationHoliday, Integer> {
    OrganizationHoliday[] findAllByHoliday_Id(Integer holidayId);

    OrganizationHoliday[] findAllByOrganization_Id(Integer organizationId);

    OrganizationHoliday findByHoliday_IdAndOrganization_Id(Integer holidayId, Integer organizationId);
}
