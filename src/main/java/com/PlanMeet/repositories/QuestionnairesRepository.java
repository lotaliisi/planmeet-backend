package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Questionnaire;
import org.springframework.data.repository.CrudRepository;

public interface QuestionnairesRepository extends CrudRepository<Questionnaire, Integer> {
    Questionnaire findTopByOrderByIdDesc();

    Questionnaire findByMeeting_Id(Integer meetingId);
}
