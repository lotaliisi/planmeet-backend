package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.BookType;
import org.springframework.data.repository.CrudRepository;

public interface BookTypeRepository extends CrudRepository<BookType, Integer> {
}
