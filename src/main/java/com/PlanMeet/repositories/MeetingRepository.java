package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Meeting;
import org.springframework.data.repository.CrudRepository;

public interface MeetingRepository extends CrudRepository<Meeting, Integer> {
    Meeting findTopByOrderByIdDesc();
}
