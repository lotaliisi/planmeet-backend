package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Equipment;
import org.springframework.data.repository.CrudRepository;

public interface EquipmentRepository extends CrudRepository<Equipment, Integer> {
    Equipment findByName(String name);
}
