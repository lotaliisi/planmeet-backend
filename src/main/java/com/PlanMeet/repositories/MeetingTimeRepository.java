package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.MeetingTime;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;

public interface MeetingTimeRepository extends CrudRepository<MeetingTime, Integer> {
    MeetingTime[] findAllByQuestionnaire_Id(Integer questionnaireId);

    MeetingTime findByQuestionnaire_IdAndStartTimeAndEndTime(
            Integer questionnaireId,
            Timestamp startTime,
            Timestamp endTime);
}
