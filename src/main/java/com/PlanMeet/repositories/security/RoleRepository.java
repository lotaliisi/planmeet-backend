package com.PlanMeet.repositories.security;

import com.PlanMeet.entity.domain.security.ERole;
import com.PlanMeet.entity.domain.security.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}
