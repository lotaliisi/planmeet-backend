package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.TalTechRoom;
import org.springframework.data.repository.CrudRepository;

public interface TalTechRoomRepository extends CrudRepository<TalTechRoom, Integer> {
    TalTechRoom[] findAllByMeeting_Id(Integer meetingId);

    TalTechRoom findTopByOrderByIdDesc();
}
