package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.Answer;
import org.springframework.data.repository.CrudRepository;

public interface AnswerRepository extends CrudRepository<Answer, Integer> {
    Answer[] findAllByQuestionnaire_Id(Integer questionnaireId);

    Answer findByQuestionnaire_IdAndName(Integer questionnaireId, String name);
}
