package com.PlanMeet.repositories;

import com.PlanMeet.entity.domain.UserOrganization;
import org.springframework.data.repository.CrudRepository;

public interface UserOrganizationRepository extends CrudRepository<UserOrganization, Integer> {
    UserOrganization[] findAllByUser_Id(Integer userId);

    UserOrganization[] findAllByOrganization_Id(Integer organizationId);

    UserOrganization findByUser_IdAndOrganization_Id(Integer userId, Integer organizationId);
}

