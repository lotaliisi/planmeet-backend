package com.PlanMeet.entity.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class OrganizationHoliday {
    @ManyToOne
    @NotBlank
    public Organization organization;
    @ManyToOne
    @NotBlank
    public Holiday holiday;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public OrganizationHoliday(Organization organization, Holiday holiday) {
        this.organization = organization;
        this.holiday = holiday;
    }
}
