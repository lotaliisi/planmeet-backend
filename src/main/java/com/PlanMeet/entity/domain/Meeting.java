package com.PlanMeet.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Meeting {
    @Size(max = 120)
    public String location;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(max = 50)
    private String name;
    @Size(max = 250)
    private String description;
    private Timestamp startTime;
    private Timestamp endTime;
    @OneToMany(mappedBy = "meeting", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<MeetingMember> meetingMembers;
    @OneToMany(mappedBy = "meeting", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<TalTechRoom> talTechRooms;
    @OneToOne(mappedBy = "meeting", cascade = CascadeType.ALL)
    @JsonIgnore
    private Questionnaire questionnaire;

    public Meeting(String name, String description, Timestamp startTime, Timestamp endTime, String location) {
        this.name = name;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
    }
}
