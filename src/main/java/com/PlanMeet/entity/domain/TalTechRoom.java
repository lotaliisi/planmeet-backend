package com.PlanMeet.entity.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class TalTechRoom {
    @ManyToOne
    public Meeting meeting = null;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(max = 20)
    private String row_num;
    @NotBlank
    @Size(max = 20)
    private String hoone;
    @NotBlank
    @Size(max = 20)
    private String ruum_id;
    @NotBlank
    @Size(max = 20)
    private String ruum_kood;
    @NotBlank
    @Size(max = 100)
    private String hoone_nimetus;
    @NotBlank
    @Size(max = 20)
    private String korrus;
    @NotBlank
    @Size(max = 20)
    private String avalik;
    @NotBlank
    @Size(max = 20)
    private String kohtade_arv;
    @NotBlank
    @Size(max = 20)
    private String vaba;
    @NotBlank
    @Size(max = 250)
    private String vaba_txt;
    @NotBlank
    @Size(max = 250)
    private String ruumi_tyyp;
    @NotBlank
    @Size(max = 250)
    private String bron_txt;
    @NotBlank
    @Size(max = 250)
    private String varustus;

    public TalTechRoom(String row_num, String hoone, String ruum_id, String ruum_kood, String hoone_nimetus,
                       String korrus, String avalik, String kohtade_arv, String vaba, String vaba_txt,
                       String ruumi_tyyp, String bron_txt, String varustus, Meeting meeting) {
        this.row_num = row_num;
        this.hoone = hoone;
        this.ruum_id = ruum_id;
        this.ruum_kood = ruum_kood;
        this.hoone_nimetus = hoone_nimetus;
        this.korrus = korrus;
        this.avalik = avalik;
        this.kohtade_arv = kohtade_arv;
        this.vaba = vaba;
        this.vaba_txt = vaba_txt;
        this.ruumi_tyyp = ruumi_tyyp;
        this.bron_txt = bron_txt;
        this.varustus = varustus;
        this.meeting = meeting;
    }

    public TalTechRoom(String row_num, String hoone, String ruum_id, String ruum_kood, String hoone_nimetus,
                       String korrus, String avalik, String kohtade_arv, String vaba, String vaba_txt,
                       String ruumi_tyyp, String bron_txt, String varustus) {
        this.row_num = row_num;
        this.hoone = hoone;
        this.ruum_id = ruum_id;
        this.ruum_kood = ruum_kood;
        this.hoone_nimetus = hoone_nimetus;
        this.korrus = korrus;
        this.avalik = avalik;
        this.kohtade_arv = kohtade_arv;
        this.vaba = vaba;
        this.vaba_txt = vaba_txt;
        this.ruumi_tyyp = ruumi_tyyp;
        this.bron_txt = bron_txt;
        this.varustus = varustus;
    }
}
