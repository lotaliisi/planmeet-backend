package com.PlanMeet.entity.domain;

import com.PlanMeet.entity.domain.security.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class UserOrganization {
    @ManyToOne
    @NotBlank
    public Organization organization;
    @ManyToOne
    @NotBlank
    public User user;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public UserOrganization(Organization organization, User user) {
        this.organization = organization;
        this.user = user;
    }
}
