package com.PlanMeet.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Set;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Holiday {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(max = 50)
    private String name;
    @NotBlank
    private boolean vocation;
    @NotBlank
    private Date date;
    @OneToMany(mappedBy = "holiday", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<OrganizationHoliday> organizationHolidays;

    public Holiday(String name, boolean vocation, Date date) {
        this.name = name;
        this.vocation = vocation;
        this.date = date;
    }
}

