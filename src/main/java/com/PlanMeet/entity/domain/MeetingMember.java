package com.PlanMeet.entity.domain;

import com.PlanMeet.entity.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class MeetingMember {
    @ManyToOne
    @NotBlank
    public Meeting meeting;
    @ManyToOne
    @NotBlank
    public User user;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private boolean organizer;
    @OneToMany(mappedBy = "meetingMember", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<MemberAnswer> memberAnswers;

    public MeetingMember(boolean organizer, Meeting meeting, User user) {
        this.organizer = organizer;
        this.meeting = meeting;
        this.user = user;
    }
}
