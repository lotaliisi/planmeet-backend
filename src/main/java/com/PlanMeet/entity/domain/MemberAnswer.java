package com.PlanMeet.entity.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class MemberAnswer {
    @ManyToOne
    @NotBlank
    public MeetingTime meetingTime;
    @ManyToOne
    @NotBlank
    public MeetingMember meetingMember;
    @ManyToOne
    @NotBlank
    public Answer answer;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public MemberAnswer(MeetingTime meetingTime, MeetingMember meetingMember, Answer answer) {
        this.meetingTime = meetingTime;
        this.meetingMember = meetingMember;
        this.answer = answer;
    }
}
