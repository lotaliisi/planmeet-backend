package com.PlanMeet.entity.domain.security;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
