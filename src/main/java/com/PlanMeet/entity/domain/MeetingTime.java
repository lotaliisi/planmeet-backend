package com.PlanMeet.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class MeetingTime {
    @ManyToOne
    @NotBlank
    public Questionnaire questionnaire;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private Timestamp startTime;
    @NotBlank
    private Timestamp endTime;
    @OneToMany(mappedBy = "meetingTime", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<MemberAnswer> memberAnswers;

    public MeetingTime(Timestamp startTime, Timestamp endTime, Questionnaire questionnaire) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.questionnaire = questionnaire;
    }
}
