package com.PlanMeet.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.sql.Date;
import java.util.Set;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Questionnaire {
    @OneToOne
    @NotBlank
    public Meeting meeting;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private Date deadline;
    @OneToMany(mappedBy = "questionnaire", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Answer> answers;
    @OneToMany(mappedBy = "questionnaire", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<MeetingTime> meetingTimes;

    public Questionnaire(Date deadline, Meeting meeting) {
        this.deadline = deadline;
        this.meeting = meeting;
    }
}
