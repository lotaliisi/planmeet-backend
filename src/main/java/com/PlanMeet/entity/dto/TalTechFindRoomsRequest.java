package com.PlanMeet.entity.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class TalTechFindRoomsRequest {
    List<String> times = new ArrayList<>();
    String room = "";
    String building = "-1"; //-1 if none selected, otherwise EIK or other code
    String floor = "-1"; //-1 if none selected, otherwise number
    String min = ""; //numbers
    String max = ""; //numbers
    String publicUse = "-1"; //jah,ei, -1
    String free = "-1"; //jah,ei, -1
    List<String> equipments = new ArrayList<>();
}
