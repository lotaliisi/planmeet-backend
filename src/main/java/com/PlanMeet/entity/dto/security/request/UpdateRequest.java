package com.PlanMeet.entity.dto.security.request;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UpdateRequest {

    private Integer id;

    private String username;

    private String email;

    private String firstName;

    private String lastName;

    private Date birthday;

    private String password;

    private Set<String> roles = new HashSet<>();
}
