package com.PlanMeet.entity.dto.security.response;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.util.List;

@Getter
@Setter
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Integer id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private Date birthday;
    private List<String> roles;

    public JwtResponse(String token, Integer id, String username, String email, String firstName, String lastName,
                       Date birthday, List<String> roles) {
        this.token = token;
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.roles = roles;
    }
}
