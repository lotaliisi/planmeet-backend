package com.PlanMeet.entity.dto;

import com.PlanMeet.entity.domain.TalTechRoom;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class TalTechBookRoomsRequest {
    List<String> times = new ArrayList<>();
    TalTechRoom room;
    String code = "";
    String comment = "";
    String personCode = "";
}
