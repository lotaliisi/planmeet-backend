package com.PlanMeet.entity.dto;

public class TalTechFindBookedRoomsResponse {
    String ajad_txt;
    String kellele;
    String kommentaar;
    String kuupaev_txt;
    String muut_kuupaev_txt;
    String muutja;
    String pcolor;
    String pohjus_txt;
    String proc;
    String row_num;
    String ruum_kood;
    String seisund_txt;
    String taotleja;
}
