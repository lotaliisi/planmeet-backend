package com.PlanMeet.entity.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TalTechFindBookedRoomsRequest {
    String j_code = "";
    String p_from_row = "0";
    String p_orderby = "DESC_muut_kuupaev";
    String p_where = "";
    String p_lisa = "";
}
