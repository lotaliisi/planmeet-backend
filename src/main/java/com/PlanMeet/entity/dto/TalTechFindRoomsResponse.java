package com.PlanMeet.entity.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TalTechFindRoomsResponse {
    String row_num;
    String hoone;
    String ruum_id;
    String ruum_kood;
    String hoone_nimetus;
    String korrus;
    String avalik;
    String kohtade_arv;
    String vaba;
    String vaba_txt;
    String ruumi_tyyp;
    String bron_txt;
    String varustus;
}
