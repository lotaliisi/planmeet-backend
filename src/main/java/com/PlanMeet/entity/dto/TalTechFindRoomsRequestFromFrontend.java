package com.PlanMeet.entity.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class TalTechFindRoomsRequestFromFrontend {
    List<String> times = new ArrayList<>();
    List<String> rooms = new ArrayList<>();
    List<String> buildings = new ArrayList<>();
    List<String> equipments = new ArrayList<>();
    String min = ""; //numbers
    String max = ""; //numbers
    boolean free; //jah,ei, -1
    boolean publicUse; //jah,ei, -1
}
