package com.PlanMeet.services;

import com.PlanMeet.entity.dto.TalTechBookRoomsRequest;
import com.PlanMeet.entity.dto.TalTechFindBookedRoomsResponse;
import com.PlanMeet.entity.dto.TalTechFindRoomsRequest;
import com.PlanMeet.entity.dto.TalTechFindRoomsResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import org.yaml.snakeyaml.util.UriEncoder;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class TalTechRoomsService {

    @Value("${oisUser}")
    private String oisUser;
    @Value("${oisPswd}")
    private String oisPswd;

    public List<TalTechFindRoomsResponse> requestRoomsFromOis(TalTechFindRoomsRequest talTechFindRoomsRequest) {
        try {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            var client = HttpClient.newBuilder()
                    .cookieHandler(cookieManager)
                    .build();

            if (login(client) && postRoomSearchParams(client, talTechFindRoomsRequest)) {
                List<TalTechFindRoomsResponse> rooms = getRooms(client);
                logout(client);
                return rooms;
            }
        } catch (IOException | InterruptedException | URISyntaxException e) {
            return null;
        }
        return null;
    }

    public TalTechFindRoomsResponse bookRoomsInOis(TalTechFindRoomsRequest findRoomRequest,
                                                   TalTechBookRoomsRequest bookRoomRequest) {
        try {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            var client = HttpClient.newBuilder()
                    .cookieHandler(cookieManager)
                    .build();

            if (login(client) && postRoomSearchParams(client, findRoomRequest)) {
                List<TalTechFindRoomsResponse> rooms = getRooms(client);
                TalTechFindRoomsResponse room = rooms.get(0);
                String[] parts = room.getBron_txt().split("'");
                String page = parts[1];
                requestRoom(page, client);
                bookRoom(client, bookRoomRequest);
                logout(client);
                return room;
            }
        } catch (IOException | InterruptedException | URISyntaxException e) {
            return null;
        }
        return null;
    }

    private boolean login(HttpClient client) throws IOException, InterruptedException {
        URI loginUri = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.page")
                .queryParam("_page", "9A46066693F9020547B19035E345EAEE")
                .queryParam("p_type", "yld")
                .queryParam("p_user", oisUser)
                .queryParam("p_pwd", oisPswd)
                .queryParam("p_mobiil", "big")
                .queryParam("p_mobiil_tel", "")
                .build()
                .toUri();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(loginUri)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.statusCode() == 302;
    }

    private boolean postRoomSearchParams(HttpClient client, TalTechFindRoomsRequest talTechFindRoomsRequest) throws IOException, InterruptedException {
        UriComponentsBuilder roomSearchUriCompBuilder = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.save_")
                .queryParam("_page", "0AADD2B9DC6893281013032621556C1A79B3E5BC4DD10D4453744FC9AA2" +
                        "6CFC76601843D6C99EEF393720949B9ACDF197AF28ECE37F72D16")
                .queryParam("rbpruum", talTechFindRoomsRequest.getRoom())
                .queryParam("rbphoone", talTechFindRoomsRequest.getBuilding())
                .queryParam("rbpkorrus", talTechFindRoomsRequest.getFloor())
                .queryParam("rbpminarv", talTechFindRoomsRequest.getMin())
                .queryParam("rbpmaxkarv", talTechFindRoomsRequest.getMax())
                .queryParam("rbpavalik", talTechFindRoomsRequest.getPublicUse())
                .queryParam("rbpavaba", talTechFindRoomsRequest.getFree());

        for (String time : talTechFindRoomsRequest.getTimes()) {
            roomSearchUriCompBuilder.queryParam("rbpajad", UriEncoder.decode(time));
        }

        for (String equipment : talTechFindRoomsRequest.getEquipments()) {
            roomSearchUriCompBuilder.queryParam("rbpvarustus", equipment);
        }

        URI roomSearchUri = roomSearchUriCompBuilder
                .build()
                .toUri();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(roomSearchUri)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.statusCode() == 200;
    }

    private List<TalTechFindRoomsResponse> getRooms(HttpClient client) throws URISyntaxException, IOException, InterruptedException {
        List<TalTechFindRoomsResponse> rooms = new ArrayList<>();

        URI getRoomsUriCompBuilder = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.get_json")
                .queryParam("j_code", "E29A2E79AD849001879D78B114C99C2B59046D4476BE41C8")
                .queryParam("p_from_row", "0")
                .queryParam("p_orderby", "")
                .queryParam("p_where", "")
                .queryParam("p_lisa", "")
                .build()
                .toUri();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(getRoomsUriCompBuilder)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        rooms.addAll(parseRoomsJson(response.body()));

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JsonNode jsonResponse = objectMapper.readTree(response.body());
        int rowCount = jsonResponse.get(0).get("rowCount").asInt();

        for (int i = 2; i < (Math.ceil((double) rowCount / 20) + 1); i++) {
            getRoomsUriCompBuilder = UriComponentsBuilder.newInstance()
                    .scheme("https")
                    .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.get_json")
                    .queryParam("j_code", "E29A2E79AD849001879D78B114C99C2B59046D4476BE41C8")
                    .queryParam("p_from_row", String.valueOf(i))
                    .queryParam("p_orderby", "")
                    .queryParam("p_where", "")
                    .queryParam("p_lisa", "")
                    .build()
                    .toUri();
            request = HttpRequest.newBuilder()
                    .uri(getRoomsUriCompBuilder)
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            rooms.addAll(parseRoomsJson(response.body()));
        }
        return rooms;
    }

    private boolean requestRoom(String page, HttpClient client) throws IOException, InterruptedException {
        URI getRoomsUriCompBuilder = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.page")
                .queryParam("_page", page)
                .build()
                .toUri();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(getRoomsUriCompBuilder)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.statusCode() == 200;
    }

    private boolean bookRoom(HttpClient client, TalTechBookRoomsRequest bookRoomRequest)
            throws IOException, InterruptedException {
        UriComponentsBuilder roomSearchUriCompBuilder = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.save_")
                .queryParam("_page", "0AADD2B9DC6893281013032621556C1AD1F4B047CE5B1FE8720B01B24F58A5C06B807FF6614D8025")
                .queryParam("rbppkood", bookRoomRequest.getCode())
                .queryParam("rbpkomment", bookRoomRequest.getComment())
                .queryParam("rbpisika", bookRoomRequest.getPersonCode())
                .queryParam("rbba", "12");

        for (String time : bookRoomRequest.getTimes()) {
            roomSearchUriCompBuilder.queryParam("rbpajad", UriEncoder.decode(time));
        }

        URI roomSearchUri = roomSearchUriCompBuilder
                .build()
                .toUri();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(roomSearchUri)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.statusCode() == 200;
    }

    private List<TalTechFindBookedRoomsResponse> getBookings(HttpClient client) throws URISyntaxException, IOException, InterruptedException {
        List<TalTechFindRoomsResponse> rooms = new ArrayList<>();

        URI getRoomsUriCompBuilder = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.get_json")
                .queryParam("j_code", "E29A2E79AD849001879D78B114C99C2B59046D4476BE41C8")
                .queryParam("p_from_row", "0")
                .queryParam("p_orderby", "")
                .queryParam("p_where", "")
                .queryParam("p_lisa", "")
                .build()
                .toUri();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(getRoomsUriCompBuilder)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        rooms.addAll(parseRoomsJson(response.body()));

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JsonNode jsonResponse = objectMapper.readTree(response.body());
        int rowCount = jsonResponse.get(0).get("rowCount").asInt();

        for (int i = 2; i < (Math.ceil((double) rowCount / 20) + 1); i++) {
            getRoomsUriCompBuilder = UriComponentsBuilder.newInstance()
                    .scheme("https")
                    .host("ois2.ttu.ee/uusois/!uus_ois2.ois_public.get_json")
                    .queryParam("j_code", "E29A2E79AD849001879D78B114C99C2B59046D4476BE41C8")
                    .queryParam("p_from_row", String.valueOf(i))
                    .queryParam("p_orderby", "")
                    .queryParam("p_where", "")
                    .queryParam("p_lisa", "")
                    .build()
                    .toUri();
            request = HttpRequest.newBuilder()
                    .uri(getRoomsUriCompBuilder)
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            rooms.addAll(parseRoomsJson(response.body()));
        }
        return new ArrayList<>();
    }

    private void logout(HttpClient client) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(
                        "https://ois2.ttu.ee/uusois/uus_ois2.ois_public.logout?p_version=big"))
                .build();
        client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    private List<TalTechFindRoomsResponse> parseRoomsJson(String roomsJson) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JsonNode response = objectMapper.readTree(roomsJson);
        JsonNode data = response.get(1).get("andmed");
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<TalTechFindRoomsResponse>>() {
        });
        List<TalTechFindRoomsResponse> roomsFromQueries = reader.readValue(data);
        return roomsFromQueries;
    }
}
