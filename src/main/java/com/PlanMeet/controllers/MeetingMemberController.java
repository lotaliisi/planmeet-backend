package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.MeetingMember;
import com.PlanMeet.repositories.MeetingMemberRepository;
import com.PlanMeet.security.services.UserDetailsImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/meeting_members")
@PreAuthorize("hasRole('USER')")
public class MeetingMemberController {
    private MeetingMemberRepository meetingMemberRepository;

    public MeetingMemberController(MeetingMemberRepository meetingMemberRepository) {
        this.meetingMemberRepository = meetingMemberRepository;
    }

    @GetMapping("")
    public Iterable<MeetingMember> getAll() {
        return meetingMemberRepository.findAll();
    }

    @GetMapping("/meeting/{meetingId}")
    public MeetingMember[] getAllByMeeting(@PathVariable Integer meetingId) {
        return meetingMemberRepository.findAllByMeeting_Id(meetingId);
    }

    @GetMapping("/user/{userId}")
    public MeetingMember[] getAllByUser(@PathVariable Integer userId) {
        return meetingMemberRepository.findAllByUser_Id(userId);
    }

    @GetMapping("/user/meeting/{meetingId}")
    public MeetingMember getByUserAndMeeting(@PathVariable Integer meetingId) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            return meetingMemberRepository.findByUser_IdAndMeeting_Id(userId, meetingId);
        }
        return null;
    }

    @GetMapping("/{id}")
    public Optional<MeetingMember> getById(@PathVariable Integer id) {
        return meetingMemberRepository.findById(id);
    }

    @PostMapping("")
    public MeetingMember create(@RequestBody MeetingMember meetingMember) {
        if (meetingMemberRepository.findByUser_IdAndMeeting_Id(
                meetingMember.getUser().getId(),
                meetingMember.getMeeting().getId()) == null) {
            return meetingMemberRepository.save(meetingMember);
        } else {
            return null;
        }
    }

    @PostMapping("/{id}")
    public MeetingMember update(@RequestBody MeetingMember meetingMember) {
        return meetingMemberRepository.save(meetingMember);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<MeetingMember> objectInDb = meetingMemberRepository.findById(id);
        objectInDb.ifPresent(object -> meetingMemberRepository.delete(object));
    }
}
