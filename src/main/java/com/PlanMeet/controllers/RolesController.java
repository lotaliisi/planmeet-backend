package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.security.ERole;
import com.PlanMeet.entity.domain.security.Role;
import com.PlanMeet.repositories.security.RoleRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/roles")
@PreAuthorize("hasRole('ADMIN')")
public class RolesController {
    private RoleRepository roleRepository;

    public RolesController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping("")
    public Iterable<Role> getAll() {
        return roleRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Role> getById(@PathVariable Integer id) {
        return roleRepository.findById(id);
    }

    @GetMapping("/name/{name}")
    public Optional<Role> getByName(@PathVariable ERole name) {
        return roleRepository.findByName(name);
    }
}
