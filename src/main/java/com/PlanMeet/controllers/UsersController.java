package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.security.User;
import com.PlanMeet.repositories.security.UserRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
@PreAuthorize("hasRole('USER')")
public class UsersController {
    private UserRepository userRepository;

    public UsersController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("")
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<User> getById(@PathVariable Integer id) {
        return userRepository.findById(id);
    }

    @GetMapping("/username/{username}")
    public Optional<User> getByUsername(@PathVariable String username) {
        return userRepository.findByUsername(username);
    }

    @GetMapping("/last")
    @PreAuthorize("hasRole('ADMIN')")
    public User getLast() {
        return userRepository.findTopByOrderByIdDesc();
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public User update(@RequestBody User user) {
        return userRepository.save(user);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<User> objectInDb = userRepository.findById(id);
        objectInDb.ifPresent(object -> userRepository.delete(object));
    }
}
