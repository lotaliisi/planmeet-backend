package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.MeetingMember;
import com.PlanMeet.entity.domain.Questionnaire;
import com.PlanMeet.repositories.MeetingMemberRepository;
import com.PlanMeet.repositories.QuestionnairesRepository;
import com.PlanMeet.security.services.UserDetailsImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/questionnaires")
@PreAuthorize("hasRole('USER')")
public class QuestionnairesController {

    private QuestionnairesRepository questionnairesRepository;
    private MeetingMemberRepository meetingMemberRepository;

    public QuestionnairesController(QuestionnairesRepository questionnairesRepository,
                                    MeetingMemberRepository meetingMemberRepository) {
        this.questionnairesRepository = questionnairesRepository;
        this.meetingMemberRepository = meetingMemberRepository;
    }

    @GetMapping("")
    public Iterable<Questionnaire> getAll() {
        List<Questionnaire> questionnaires = new ArrayList<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            for (Questionnaire questionnaire : questionnairesRepository.findAll()) {
                MeetingMember member = meetingMemberRepository.findByUser_IdAndMeeting_Id(userId,
                        questionnaire.getMeeting().getId());
                if (member != null) {
                    questionnaires.add(questionnaire);
                }
            }
        }
        return questionnaires;
    }

    @GetMapping("/{id}")
    public Optional<Questionnaire> getById(@PathVariable Integer id) {
        return questionnairesRepository.findById(id);
    }

    @GetMapping("/last")
    public Questionnaire getLast() {
        return questionnairesRepository.findTopByOrderByIdDesc();
    }

    @GetMapping("/meeting/{meetingId}")
    public Questionnaire getByMeeting(@PathVariable Integer meetingId) {
        return questionnairesRepository.findByMeeting_Id(meetingId);
    }

    @PostMapping("")
    public Questionnaire create(@RequestBody Questionnaire questionnaire) {
        return questionnairesRepository.save(questionnaire);
    }

    @PostMapping("/{id}")
    public Questionnaire update(@RequestBody Questionnaire questionnaire) {
        return questionnairesRepository.save(questionnaire);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<Questionnaire> objectInDb = questionnairesRepository.findById(id);
        objectInDb.ifPresent(object -> questionnairesRepository.delete(object));
    }
}
