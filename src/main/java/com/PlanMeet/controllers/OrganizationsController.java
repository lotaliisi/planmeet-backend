package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Organization;
import com.PlanMeet.repositories.OrganizationRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/organizations")
@PreAuthorize("hasRole('USER')")
public class OrganizationsController {
    private OrganizationRepository organizationRepository;

    public OrganizationsController(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @GetMapping("")
    public Iterable<Organization> getAll() {
        return organizationRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Organization> getById(@PathVariable Integer id) {
        return organizationRepository.findById(id);
    }

    @GetMapping("/name/{name}")
    public Organization getByName(@PathVariable String name) {
        return organizationRepository.findByName(name);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Organization create(@RequestBody Organization organization) {
        Organization organization1 = organizationRepository.save(organization);
        return organization1;
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Organization update(@RequestBody Organization organization) {
        return organizationRepository.save(organization);

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<Organization> organizationInDb = organizationRepository.findById(id);
        organizationInDb.ifPresent(organization -> organizationRepository.delete(organization));
    }
}
