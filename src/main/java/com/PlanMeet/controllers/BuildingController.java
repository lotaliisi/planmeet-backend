package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Building;
import com.PlanMeet.repositories.BuildingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/buildings")
@PreAuthorize("hasRole('USER')")
public class BuildingController {
    private BuildingRepository buildingRepository;

    public BuildingController(BuildingRepository buildingRepository) {
        this.buildingRepository = buildingRepository;
    }

    @GetMapping("")
    public Iterable<Building> getAll() {
        return buildingRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Building> getById(@PathVariable Integer id) {
        return buildingRepository.findById(id);
    }

    @GetMapping("/name/{name}")
    public Building getByName(@PathVariable String name) {
        return buildingRepository.findByName(name);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Building create(@RequestBody Building building) {
        return buildingRepository.save(building);
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Building update(@RequestBody Building building) {
        return buildingRepository.save(building);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<Building> objectInDb = buildingRepository.findById(id);
        objectInDb.ifPresent(object -> buildingRepository.delete(object));
    }
}
