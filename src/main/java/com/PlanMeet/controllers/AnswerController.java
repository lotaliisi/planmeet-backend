package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Answer;
import com.PlanMeet.repositories.AnswerRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/answers")
@PreAuthorize("hasRole('USER')")
public class AnswerController {
    private AnswerRepository answerRepository;

    public AnswerController(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Iterable<Answer> getAll() {
        return answerRepository.findAll();
    }

    @GetMapping("/questionnaire/{questionnaireId}")
    public Answer[] getAllByQuestionnaire(@PathVariable Integer questionnaireId) {
        return answerRepository.findAllByQuestionnaire_Id(questionnaireId);
    }

    @GetMapping("/{id}")
    public Optional<Answer> getById(@PathVariable Integer id) {
        return answerRepository.findById(id);
    }

    @PostMapping("")
    public Answer create(@RequestBody Answer answer) {
        if (answerRepository.findByQuestionnaire_IdAndName(
                answer.getQuestionnaire().getId(), answer.getName()) == null) {
            return answerRepository.save(answer);
        } else {
            return null;
        }
    }

    @PostMapping("/{id}")
    public Answer update(@RequestBody Answer questionnaire) {
        return answerRepository.save(questionnaire);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<Answer> objectInDb = answerRepository.findById(id);
        objectInDb.ifPresent(object -> answerRepository.delete(object));
    }
}
