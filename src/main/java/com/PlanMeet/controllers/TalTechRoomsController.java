package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.TalTechRoom;
import com.PlanMeet.entity.dto.TalTechBookRoomsRequest;
import com.PlanMeet.entity.dto.TalTechFindRoomsRequest;
import com.PlanMeet.entity.dto.TalTechFindRoomsRequestFromFrontend;
import com.PlanMeet.entity.dto.TalTechFindRoomsResponse;
import com.PlanMeet.repositories.BuildingRepository;
import com.PlanMeet.repositories.TalTechRoomRepository;
import com.PlanMeet.services.TalTechRoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/taltech")
@PreAuthorize("hasRole('USER')")
public class TalTechRoomsController {

    @Autowired
    private TalTechRoomsService talTechRoomsService;
    private BuildingRepository buildingRepository;
    private TalTechRoomRepository talTechRoomRepository;

    public TalTechRoomsController(BuildingRepository buildingRepository, TalTechRoomRepository talTechRoomRepository) {
        this.buildingRepository = buildingRepository;
        this.talTechRoomRepository = talTechRoomRepository;
    }

    @GetMapping("/{id}")
    public Optional<TalTechRoom> getById(@PathVariable Integer id) {
        return talTechRoomRepository.findById(id);
    }

    @GetMapping("/last")
    public TalTechRoom getLast() {
        return talTechRoomRepository.findTopByOrderByIdDesc();
    }

    @GetMapping("/meeting/{meetingId}")
    public TalTechRoom[] getAllByMeeting(@PathVariable Integer meetingId) {
        return talTechRoomRepository.findAllByMeeting_Id(meetingId);
    }

    @PostMapping("")
    public TalTechRoom create(@RequestBody TalTechRoom talTechRoom) {
        return talTechRoomRepository.save(talTechRoom);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<TalTechRoom> objectInDb = talTechRoomRepository.findById(id);
        objectInDb.ifPresent(object -> talTechRoomRepository.delete(object));
    }

    @PostMapping("/book")
    public TalTechFindRoomsResponse bookRooms(@RequestBody TalTechBookRoomsRequest request) {
        TalTechFindRoomsRequest talTechFindRoomsRequest = new TalTechFindRoomsRequest();
        talTechFindRoomsRequest.setTimes(request.getTimes());
        talTechFindRoomsRequest.setRoom(request.getRoom().getRuum_kood());
        talTechFindRoomsRequest.setBuilding(request.getRoom().getHoone());
        return talTechRoomsService.bookRoomsInOis(talTechFindRoomsRequest, request);
    }

    @PostMapping("/find")
    public List<TalTechFindRoomsResponse> requestRooms(@RequestBody TalTechFindRoomsRequestFromFrontend query) {
        List<TalTechFindRoomsResponse> rooms = new ArrayList<>();
        for (String room : query.getRooms()) {
            for (String building : query.getBuildings()) {
                String buildingCode = buildingRepository.findByName(building).getCode();
                TalTechFindRoomsRequest talTechFindRoomsRequest = new TalTechFindRoomsRequest();
                List<String> times = new ArrayList<>();
                times.addAll(query.getTimes());
                talTechFindRoomsRequest.setTimes(times);
                talTechFindRoomsRequest.setRoom(room);
                talTechFindRoomsRequest.setBuilding(buildingCode);
                talTechFindRoomsRequest.setFloor("-1");
                talTechFindRoomsRequest.setMin(query.getMin());
                talTechFindRoomsRequest.setMax(query.getMax());
                if (query.isPublicUse()) {
                    talTechFindRoomsRequest.setPublicUse("jah");
                } else {
                    talTechFindRoomsRequest.setPublicUse("ei");
                }
                if (query.isFree()) {
                    talTechFindRoomsRequest.setFree("jah");
                } else {
                    talTechFindRoomsRequest.setFree("ei");
                }
                List<String> equipments = new ArrayList<>();
                equipments.addAll(query.getEquipments());
                talTechFindRoomsRequest.setEquipments(equipments);
                List<TalTechFindRoomsResponse> response = talTechRoomsService.requestRoomsFromOis(talTechFindRoomsRequest);
                rooms.addAll(response);
            }
            if (query.getBuildings().isEmpty()) {
                TalTechFindRoomsRequest talTechFindRoomsRequest = new TalTechFindRoomsRequest();
                List<String> times = new ArrayList<>();
                times.addAll(query.getTimes());
                talTechFindRoomsRequest.setTimes(times);
                talTechFindRoomsRequest.setRoom(room);
                talTechFindRoomsRequest.setBuilding("-1");
                talTechFindRoomsRequest.setFloor("-1");
                talTechFindRoomsRequest.setMin(query.getMin());
                talTechFindRoomsRequest.setMax(query.getMax());
                if (query.isPublicUse()) {
                    talTechFindRoomsRequest.setPublicUse("jah");
                } else {
                    talTechFindRoomsRequest.setPublicUse("ei");
                }
                if (query.isFree()) {
                    talTechFindRoomsRequest.setFree("jah");
                } else {
                    talTechFindRoomsRequest.setFree("ei");
                }
                List<String> equipments = new ArrayList<>();
                equipments.addAll(query.getEquipments());
                talTechFindRoomsRequest.setEquipments(equipments);
                List<TalTechFindRoomsResponse> response = talTechRoomsService.requestRoomsFromOis(talTechFindRoomsRequest);
                rooms.addAll(response);
            }
        }
        if (query.getRooms().isEmpty()) {
            for (String building : query.getBuildings()) {
                String buildingCode = buildingRepository.findByName(building).getCode();
                TalTechFindRoomsRequest talTechFindRoomsRequest = new TalTechFindRoomsRequest();
                List<String> times = new ArrayList<>();
                times.addAll(query.getTimes());
                talTechFindRoomsRequest.setTimes(times);
                talTechFindRoomsRequest.setRoom("");
                talTechFindRoomsRequest.setBuilding(buildingCode);
                talTechFindRoomsRequest.setFloor("-1");
                talTechFindRoomsRequest.setMin(query.getMin());
                talTechFindRoomsRequest.setMax(query.getMax());
                if (query.isPublicUse()) {
                    talTechFindRoomsRequest.setPublicUse("jah");
                } else {
                    talTechFindRoomsRequest.setPublicUse("ei");
                }
                if (query.isFree()) {
                    talTechFindRoomsRequest.setFree("jah");
                } else {
                    talTechFindRoomsRequest.setFree("ei");
                }
                List<String> equipments = new ArrayList<>();
                equipments.addAll(query.getEquipments());
                talTechFindRoomsRequest.setEquipments(equipments);
                List<TalTechFindRoomsResponse> response = talTechRoomsService.requestRoomsFromOis(talTechFindRoomsRequest);
                rooms.addAll(response);
            }
            if (query.getBuildings().isEmpty()) {
                TalTechFindRoomsRequest talTechFindRoomsRequest = new TalTechFindRoomsRequest();
                List<String> times = new ArrayList<>();
                times.addAll(query.getTimes());
                talTechFindRoomsRequest.setTimes(times);
                talTechFindRoomsRequest.setRoom("");
                talTechFindRoomsRequest.setBuilding("-1");
                talTechFindRoomsRequest.setFloor("-1");
                talTechFindRoomsRequest.setMin(query.getMin());
                talTechFindRoomsRequest.setMax(query.getMax());
                if (query.isPublicUse()) {
                    talTechFindRoomsRequest.setPublicUse("jah");
                } else {
                    talTechFindRoomsRequest.setPublicUse("ei");
                }
                if (query.isFree()) {
                    talTechFindRoomsRequest.setFree("jah");
                } else {
                    talTechFindRoomsRequest.setFree("ei");
                }
                List<String> equipments = new ArrayList<>();
                equipments.addAll(query.getEquipments());
                talTechFindRoomsRequest.setEquipments(equipments);
                List<TalTechFindRoomsResponse> response = talTechRoomsService.requestRoomsFromOis(talTechFindRoomsRequest);
                rooms.addAll(response);
            }
        }
        return rooms;
    }
}
