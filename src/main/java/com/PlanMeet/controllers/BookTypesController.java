package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.BookType;
import com.PlanMeet.repositories.BookTypeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/book_types")
public class BookTypesController {
    private BookTypeRepository bookTypeRepository;

    public BookTypesController(BookTypeRepository bookTypeRepository) {
        this.bookTypeRepository = bookTypeRepository;
    }

    @GetMapping("")
    public Iterable<BookType> getAll() {
        return bookTypeRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<BookType> getById(@PathVariable Integer id) {
        return bookTypeRepository.findById(id);
    }

    @PostMapping("")
    public BookType create(@RequestBody BookType bookType) {
        return bookTypeRepository.save(bookType);
    }

    @PostMapping("/{id}")
    public BookType update(@RequestBody BookType bookType) {
        return bookTypeRepository.save(bookType);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<BookType> objectInDb = bookTypeRepository.findById(id);
        objectInDb.ifPresent(object -> bookTypeRepository.delete(object));
    }
}
