package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.OrganizationHoliday;
import com.PlanMeet.repositories.OrganizationHolidayRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/organization_holidays")
@PreAuthorize("hasRole('USER')")
public class OrganizationHolidayController {
    private OrganizationHolidayRepository organizationHolidayRepository;

    public OrganizationHolidayController(OrganizationHolidayRepository organizationHolidayRepository) {
        this.organizationHolidayRepository = organizationHolidayRepository;
    }

    @GetMapping("")
    public Iterable<OrganizationHoliday> getAll() {
        return organizationHolidayRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<OrganizationHoliday> getById(@PathVariable Integer id) {
        return organizationHolidayRepository.findById(id);
    }

    @GetMapping("/holiday/{holidayId}")
    public OrganizationHoliday[] getAllByHoliday(@PathVariable Integer holidayId) {
        return organizationHolidayRepository.findAllByHoliday_Id(holidayId);
    }

    @GetMapping("/organization/{organizationId}")
    public OrganizationHoliday[] getAllByOrganization(@PathVariable Integer organizationId) {
        return organizationHolidayRepository.findAllByOrganization_Id(organizationId);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public OrganizationHoliday create(@RequestBody OrganizationHoliday organization) {
        if (organizationHolidayRepository.findByHoliday_IdAndOrganization_Id(
                organization.getHoliday().getId(), organization.getOrganization().getId()) == null) {
            return organizationHolidayRepository.save(organization);
        } else {
            return null;
        }
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public OrganizationHoliday update(@RequestBody OrganizationHoliday organization) {
        return organizationHolidayRepository.save(organization);

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<OrganizationHoliday> objectInDb = organizationHolidayRepository.findById(id);
        objectInDb.ifPresent(object -> organizationHolidayRepository.delete(object));
    }
}
