package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Meeting;
import com.PlanMeet.entity.domain.MeetingMember;
import com.PlanMeet.repositories.MeetingMemberRepository;
import com.PlanMeet.repositories.MeetingRepository;
import com.PlanMeet.security.services.UserDetailsImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/meetings")
@PreAuthorize("hasRole('USER')")
public class MeetingController {

    private MeetingRepository meetingRepository;
    private MeetingMemberRepository meetingMemberRepository;

    public MeetingController(MeetingRepository meetingRepository, MeetingMemberRepository meetingMemberRepository) {
        this.meetingRepository = meetingRepository;
        this.meetingMemberRepository = meetingMemberRepository;
    }

    @GetMapping("")
    public Iterable<Meeting> getAll() {
        List<Meeting> meetings = new ArrayList<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            for (Meeting meeting : meetingRepository.findAll()) {
                MeetingMember member = meetingMemberRepository.findByUser_IdAndMeeting_Id(userId, meeting.getId());
                if (member != null) {
                    meetings.add(meeting);
                }
            }
        }
        return meetings;
    }

    @GetMapping("/{id}")
    public Optional<Meeting> getMeetingById(@PathVariable Integer id) {
        return meetingRepository.findById(id);
    }

    @GetMapping("/last")
    public Meeting getLast() {
        return meetingRepository.findTopByOrderByIdDesc();
    }

    @PostMapping("")
    public Meeting createMeeting(@RequestBody Meeting meeting) {
        return meetingRepository.save(meeting);
    }

    @PostMapping("/{id}")
    public Meeting update(@RequestBody Meeting meeting) {
        return meetingRepository.save(meeting);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            MeetingMember member = meetingMemberRepository.findByUser_IdAndMeeting_Id(userId, id);
            if (member != null && member.isOrganizer()) {
                Optional<Meeting> objectInDb = meetingRepository.findById(id);
                objectInDb.ifPresent(object -> meetingRepository.delete(object));
            }
        }
    }
}
