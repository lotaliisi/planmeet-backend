package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.MeetingTime;
import com.PlanMeet.repositories.MeetingTimeRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/meeting_times")
@PreAuthorize("hasRole('USER')")
public class MeetingTimeController {
    private MeetingTimeRepository meetingTimeRepository;

    public MeetingTimeController(MeetingTimeRepository meetingTimeRepository) {
        this.meetingTimeRepository = meetingTimeRepository;
    }

    @GetMapping("")
    public Iterable<MeetingTime> getAll() {
        return meetingTimeRepository.findAll();
    }

    @GetMapping("/questionnaire/{questionnaireId}")
    public MeetingTime[] getAllByQuestionnaire(@PathVariable Integer questionnaireId) {
        return meetingTimeRepository.findAllByQuestionnaire_Id(questionnaireId);
    }

    @GetMapping("/{id}")
    public Optional<MeetingTime> getById(@PathVariable Integer id) {
        return meetingTimeRepository.findById(id);
    }

    @PostMapping("")
    public MeetingTime create(@RequestBody MeetingTime meetingTime) {
        if (meetingTimeRepository.findByQuestionnaire_IdAndStartTimeAndEndTime(
                meetingTime.getQuestionnaire().getId(), meetingTime.getStartTime(), meetingTime.getEndTime()) == null) {
            return meetingTimeRepository.save(meetingTime);
        } else {
            return null;
        }
    }

    @PostMapping("/{id}")
    public MeetingTime update(@RequestBody MeetingTime questionnaire) {
        return meetingTimeRepository.save(questionnaire);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<MeetingTime> objectInDb = meetingTimeRepository.findById(id);
        objectInDb.ifPresent(object -> meetingTimeRepository.delete(object));
    }
}
