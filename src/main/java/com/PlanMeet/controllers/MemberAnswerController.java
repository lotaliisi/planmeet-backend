package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.MemberAnswer;
import com.PlanMeet.repositories.MemberAnswerRepository;
import com.PlanMeet.security.services.UserDetailsImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/member_answers")
@PreAuthorize("hasRole('USER')")
public class MemberAnswerController {
    private MemberAnswerRepository memberAnswerRepository;

    public MemberAnswerController(MemberAnswerRepository memberAnswerRepository) {
        this.memberAnswerRepository = memberAnswerRepository;
    }

    @GetMapping("")
    public Iterable<MemberAnswer> getAll() {
        return memberAnswerRepository.findAll();
    }

    @GetMapping("/meeting/{meetingId}")
    public MemberAnswer[] getAllByQuestionnaire(@PathVariable Integer meetingId) {
        return memberAnswerRepository.findAllByMeetingMember_Meeting_Id(meetingId);
    }

    @GetMapping("/user/meeting/{meetingId}")
    public MemberAnswer[] getAllByUserAndQuestionnaire(@PathVariable Integer meetingId) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            return memberAnswerRepository.findAllByMeetingMember_User_IdAndMeetingMember_Meeting_Id(userId,
                    meetingId);
        }
        return null;
    }

    @GetMapping("/user/{userId}")
    public MemberAnswer[] getAllByMeetingMember(@PathVariable Integer userId) {
        return memberAnswerRepository.findAllByMeetingMember_Id(userId);
    }

    @GetMapping("/{id}")
    public Optional<MemberAnswer> getById(@PathVariable Integer id) {
        return memberAnswerRepository.findById(id);
    }

    @PostMapping("")
    public MemberAnswer create(@RequestBody MemberAnswer memberAnswer) {
        if (memberAnswerRepository.findByMeetingTime_IdAndMeetingMember_Id(
                memberAnswer.getMeetingTime().getId(), memberAnswer.getMeetingMember().getId()) == null) {
            return memberAnswerRepository.save(memberAnswer);
        } else {
            MemberAnswer updatedAnswer = memberAnswerRepository.findByMeetingTime_IdAndMeetingMember_Id(
                    memberAnswer.getMeetingTime().getId(), memberAnswer.getMeetingMember().getId());
            updatedAnswer.answer = memberAnswer.answer;
            return memberAnswerRepository.save(updatedAnswer);
        }
    }

    @PostMapping("/{id}")
    public MemberAnswer update(@RequestBody MemberAnswer memberAnswer) {
        return memberAnswerRepository.save(memberAnswer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        Optional<MemberAnswer> objectInDb = memberAnswerRepository.findById(id);
        objectInDb.ifPresent(object -> memberAnswerRepository.delete(object));
    }
}
