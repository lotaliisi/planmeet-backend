package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Holiday;
import com.PlanMeet.entity.domain.OrganizationHoliday;
import com.PlanMeet.entity.domain.UserOrganization;
import com.PlanMeet.repositories.HolidayRepository;
import com.PlanMeet.repositories.OrganizationHolidayRepository;
import com.PlanMeet.repositories.UserOrganizationRepository;
import com.PlanMeet.security.services.UserDetailsImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/holidays")
@PreAuthorize("hasRole('USER')")
public class HolidayController {
    private HolidayRepository holidayRepository;
    private OrganizationHolidayRepository organizationHolidayRepository;
    private UserOrganizationRepository userOrganizationRepository;

    public HolidayController(HolidayRepository holidayRepository,
                             OrganizationHolidayRepository organizationHolidayRepository,
                             UserOrganizationRepository userOrganizationRepository) {
        this.holidayRepository = holidayRepository;
        this.organizationHolidayRepository = organizationHolidayRepository;
        this.userOrganizationRepository = userOrganizationRepository;
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public Iterable<Holiday> getAllAdmin() {
        return holidayRepository.findAll();
    }

    @GetMapping("")
    public Iterable<Holiday> getAll() {
        List<Holiday> holidays = new ArrayList<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetailsImpl) {
            Integer userId = ((UserDetailsImpl) principal).getId();
            for (Holiday holiday : holidayRepository.findAll()) {
                OrganizationHoliday[] organizationHolidays = organizationHolidayRepository.findAllByHoliday_Id(holiday.getId());
                for (OrganizationHoliday organizationHoliday : organizationHolidays) {
                    UserOrganization userOrganization = userOrganizationRepository
                            .findByUser_IdAndOrganization_Id(userId, organizationHoliday.organization.getId());
                    if (userOrganization != null) {
                        holidays.add(holiday);
                    }
                }
            }
        }
        return holidays;
    }

    @GetMapping("/{id}")
    public Optional<Holiday> getById(@PathVariable Integer id) {
        return holidayRepository.findById(id);
    }

    @GetMapping("/last")
    @PreAuthorize("hasRole('ADMIN')")
    public Holiday getLast() {
        return holidayRepository.findTopByOrderByIdDesc();
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Holiday create(@RequestBody Holiday holiday) {
        return holidayRepository.save(holiday);
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Holiday update(@RequestBody Holiday holiday) {
        return holidayRepository.save(holiday);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<Holiday> holidayInDb = holidayRepository.findById(id);
        holidayInDb.ifPresent(holiday -> holidayRepository.delete(holiday));
    }
}
