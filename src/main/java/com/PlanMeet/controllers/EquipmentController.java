package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.Equipment;
import com.PlanMeet.repositories.EquipmentRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/equipments")
@PreAuthorize("hasRole('USER')")
public class EquipmentController {
    private EquipmentRepository equipmentRepository;

    public EquipmentController(EquipmentRepository equipmentRepository) {
        this.equipmentRepository = equipmentRepository;
    }

    @GetMapping("")
    public Iterable<Equipment> getAll() {
        return equipmentRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Equipment> getById(@PathVariable Integer id) {
        return equipmentRepository.findById(id);
    }

    @GetMapping("/name/{name}")
    public Equipment getByName(@PathVariable String name) {
        return equipmentRepository.findByName(name);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Equipment create(@RequestBody Equipment equipment) {
        return equipmentRepository.save(equipment);
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Equipment update(@RequestBody Equipment equipment) {
        return equipmentRepository.save(equipment);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<Equipment> objectInDb = equipmentRepository.findById(id);
        objectInDb.ifPresent(object -> equipmentRepository.delete(object));
    }
}
