package com.PlanMeet.controllers;

import com.PlanMeet.entity.domain.UserOrganization;
import com.PlanMeet.repositories.UserOrganizationRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user_organizations")
@PreAuthorize("hasRole('USER')")
public class UserOrganizationsController {
    private UserOrganizationRepository userOrganizationRepository;

    public UserOrganizationsController(UserOrganizationRepository userOrganizationRepository) {
        this.userOrganizationRepository = userOrganizationRepository;
    }

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public Iterable<UserOrganization> getAll() {
        return userOrganizationRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<UserOrganization> getById(@PathVariable Integer id) {
        return userOrganizationRepository.findById(id);
    }

    @GetMapping("/user/{userId}")
    public UserOrganization[] getAllByUser(@PathVariable Integer userId) {
        return userOrganizationRepository.findAllByUser_Id(userId);
    }

    @GetMapping("/organization/{organizationId}")
    public UserOrganization[] getAllByOrganization(@PathVariable Integer organizationId) {
        return userOrganizationRepository.findAllByOrganization_Id(organizationId);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public UserOrganization create(@RequestBody UserOrganization userOrganization) {
        if (userOrganizationRepository.findByUser_IdAndOrganization_Id(
                userOrganization.getUser().getId(), userOrganization.getOrganization().getId()) == null) {
            return userOrganizationRepository.save(userOrganization);
        } else {
            return null;
        }
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public UserOrganization update(@RequestBody UserOrganization userOrganization) {
        return userOrganizationRepository.save(userOrganization);

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Integer id) {
        Optional<UserOrganization> objectInDb = userOrganizationRepository.findById(id);
        objectInDb.ifPresent(object -> userOrganizationRepository.delete(object));
    }
}
