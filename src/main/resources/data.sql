INSERT INTO role(name) VALUES('ROLE_USER');
INSERT INTO role(name) VALUES('ROLE_ADMIN');

INSERT INTO user(birthday, email, first_name, last_name, password, username)
VALUES('1990-01-01', 'admin@gmail.com', 'First', 'Last', '$2a$10$1T7YivjezkJyenBV05iBBeNmAhc9kbe9GOsEQ6oSfV1Ljmi0ivvXi', 'admin');
INSERT INTO user(birthday, email, first_name, last_name, password, username)
VALUES('2000-01-01', 'user@gmail.com', 'First', 'Last', '$2a$10$Ek2IQngxjeWdD/5Qp073zORkWi/HEkNRACrd7RzKCpJl7Lcryo5Gu', 'user');

INSERT INTO user_role(role_id, user_id) VALUES((SELECT id FROM role WHERE name = 'ROLE_USER'),
                                               (SELECT id FROM user WHERE username = 'user'));
INSERT INTO user_role(role_id, user_id) VALUES((SELECT id FROM role WHERE name = 'ROLE_USER'),
                                               (SELECT id FROM user WHERE username = 'admin'));
INSERT INTO user_role(role_id, user_id) VALUES((SELECT id FROM role WHERE name = 'ROLE_ADMIN'),
                                               (SELECT id FROM user WHERE username = 'admin'));

INSERT INTO organization (name) VALUES ('T-Teater');
INSERT INTO organization (name) VALUES ('Tallinna Tehnikaülikooli Kammerkoor');
INSERT INTO organization (name) VALUES ('Kuljus');
INSERT INTO organization (name) VALUES ('TTÜ Kultuuriklubi');
INSERT INTO organization (name) VALUES ('TalTech filmiklubi');
INSERT INTO organization (name) VALUES ('TalTech Robotiklubi');
INSERT INTO organization (name) VALUES ('TalTech Turundusklubi');
INSERT INTO organization (name) VALUES ('BEST-Estonia');
INSERT INTO organization (name) VALUES ('Lapikud');
INSERT INTO organization (name) VALUES ('TalTech Väitlusklubi');

INSERT INTO user_organization(user_id, organization_id) VALUES((SELECT id FROM user WHERE username = 'user'),
                                               (SELECT id FROM organization WHERE name = 'T-Teater'));
INSERT INTO user_organization(user_id, organization_id) VALUES((SELECT id FROM user WHERE username = 'user'),
                                               (SELECT id FROM organization WHERE name = 'Lapikud'));

INSERT INTO building (code, name) VALUES ('KOPLI101', 'EMEK Diislilabor');
INSERT INTO building (code, name) VALUES ('EHIT5_7', 'Energeetikamaja	');
INSERT INTO building (code, name) VALUES ('EHIT5_NG', 'Geoloogia instituut');
INSERT INTO building (code, name) VALUES ('EIK', 'IT kolledž');
INSERT INTO building (code, name) VALUES ('TEHN3', 'Infotehnoloogia maja');
INSERT INTO building (code, name) VALUES ('AKAD21F', 'Kuressaare Kolledž');
INSERT INTO building (code, name) VALUES ('AKAD21B', 'Küberneetika instituudi hoone');
INSERT INTO building (code, name) VALUES ('AKAD15', 'Loodusteaduste maja');
INSERT INTO building (code, name) VALUES ('RAJA15', 'MEKTORY maja');
INSERT INTO building (code, name) VALUES ('AKAD3', 'Maepealse maja');
INSERT INTO building (code, name) VALUES ('KOPLI101', 'Majandusmaja');
INSERT INTO building (code, name) VALUES ('KOPLI116', 'Mereakadeemia');
INSERT INTO building (code, name) VALUES ('AKAD19/1', 'Puidumaja');
INSERT INTO building (code, name) VALUES ('AKAD1', 'Raamatukogu');
INSERT INTO building (code, name) VALUES ('TK', 'Tallinna Kolledž');
INSERT INTO building (code, name) VALUES ('EHIT4_TX', 'Tekstiilimaja');
INSERT INTO building (code, name) VALUES ('KJARVE2', 'Virumaa Kolledž');
INSERT INTO building (code, name) VALUES ('EHIT5_1', 'Õppehoone U01');
INSERT INTO building (code, name) VALUES ('EHIT5_2', 'Õppehoone U02');
INSERT INTO building (code, name) VALUES ('EHIT5_2B', 'Õppehoone U02 (B korpus)');
INSERT INTO building (code, name) VALUES ('EHIT5_3', 'Õppehoone U03');
INSERT INTO building (code, name) VALUES ('EHIT5_4', 'Õppehoone U04');
INSERT INTO building (code, name) VALUES ('EHIT5_4B', 'Õppehoone U04 (B korpus)');
INSERT INTO building (code, name) VALUES ('EHIT5_5', 'Õppehoone U05');
INSERT INTO building (code, name) VALUES ('EHIT5_5B', 'Õppehoone U05 (B korpus)');
INSERT INTO building (code, name) VALUES ('EHIT5_6', 'Õppehoone U06');
INSERT INTO building (code, name) VALUES ('EHIT5_6A', 'Õppehoone U06 (A korpus)');

INSERT INTO equipment (name) VALUES ('Arvutiga töökoht');
INSERT INTO equipment (name) VALUES ('Dataprojektor');
INSERT INTO equipment (name) VALUES ('Dokumendikaamera');
INSERT INTO equipment (name) VALUES ('Helivõimendus');
INSERT INTO equipment (name) VALUES ('Kaldauditoorium');
INSERT INTO equipment (name) VALUES ('Kriiditahvel');
INSERT INTO equipment (name) VALUES ('LCD ekraan / TV');
INSERT INTO equipment (name) VALUES ('Mikrofon');
INSERT INTO equipment (name) VALUES ('Mööbli liigutamise võimalus');
INSERT INTO equipment (name) VALUES ('Reguleeritav valgustus');
INSERT INTO equipment (name) VALUES ('Tahvel');
INSERT INTO equipment (name) VALUES ('Videosalvestus');

INSERT INTO book_type (code, name) VALUES ('RUUMBRONP_K', 'Konsultatsioon');
INSERT INTO book_type (code, name) VALUES ('RUUMBRONP_E', 'Eksam');
INSERT INTO book_type (code, name) VALUES ('RUUMBRONP_OT', 'Õppe- ja teadustöö');
INSERT INTO book_type (code, name) VALUES ('RUUMBRONP_MUU', 'Muu');

